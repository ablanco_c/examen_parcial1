import { Component, OnInit } from '@angular/core';
import { Data } from '../../models/data';
import { DataService } from '../../services/data.service';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-data-details',
  templateUrl: './data-details.page.html',
  styleUrls: ['./data-details.page.scss'],
})
export class DataDetailsPage implements OnInit {

  dato : Data = {

    Nombre: '',
    Apellido: '',
    Peso: '',
    Edad: '',
    Foto: ''
  };
  datoId = null;

  constructor(private route: ActivatedRoute, private nav: NavController,
    private dataService: DataService, private loadingController: LoadingController) {

  }
  ngOnInit() {
    this.datoId = this.route.snapshot.params['id'];
    if (this.datoId) {
      this.loadDato();
    }
  }
  async loadDato() {
    const loading = await this.loadingController.create({
      message: "Cargando"
    });
    await loading.present();
    this.dataService.getData(this.datoId).subscribe(res => {
      loading.dismiss();
      this.dato = res;
      console.log(this.dato);
    })
  }



}
