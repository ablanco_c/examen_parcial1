// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
      apiKey: "AIzaSyCAlqpR3aBFTUXPmAJT8vSY2KMVrio1L94",
      authDomain: "data-b37e4.firebaseapp.com",
      databaseURL: "https://data-b37e4.firebaseio.com",
      projectId: "data-b37e4",
      storageBucket: "data-b37e4.appspot.com",
      messagingSenderId: "902816083945",
      appId: "1:902816083945:web:2562fecfff9652507d6578"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
